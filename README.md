# Mop

It is the basic parent pom to release a project only on gitlab.

## How to use

### Mandatory configuration

* Mandatory property **projectId** project's redmine id
* Mandatory property **platform** platform's the group in gitlab (chorem, nuiton or codelutin) 
* Mandatory property **platformDomain** platformDomain's is the domain of the plateform (chorem.com, nuiton.org or codelutin.com)

Or you can use the specific profile :
* nuiton
* chorem
* codelutin

### Just do release

``mvn release:prepare``

``mvn release:perform``

### Configure on gitlab ci 

Let's see the .gitlab-ci.yml
